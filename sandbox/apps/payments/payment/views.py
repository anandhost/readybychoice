import requests
from .forms import PaymentForm, SubscribeForm
from .models import PaymentId
from django.shortcuts import render
from django.conf import settings
from django.core.mail import send_mail
import requests
import json
from django.http import HttpResponseRedirect, HttpResponse

def payInfo(request):
    info = PaymentId.objects.all()
    return render(request, 'payment/customerpay.html', {'info': info})

def index(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = PaymentForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # print form.cleaned_data['name']
            # print form.cleaned_data['purpose']
            # print form.cleaned_data['amount']
            # print form.cleaned_data['phone']
            # print form.cleaned_data['email']

            # instamojo
            headers = { "X-Api-Key": "test_eea66b45729f5d1e9d6fbf34083", "X-Auth-Token": "test_6e94dfdaa46c1eff191ab85419d"}
            payload = {
              'purpose': form.cleaned_data['purpose'],
              'amount': form.cleaned_data['amount'],
              'buyer_name': form.cleaned_data['name'],
              'email': form.cleaned_data['email'],
              'phone': form.cleaned_data['phone'],
              'redirect_url': 'http://159.65.155.242:8000/en-gb/payment/pay/',
              'send_email': False,
              'send_sms': False,
              'webhook': 'http://instamojo.dev/webhook/',
              'allow_repeated_payments': False,
            }
            data = requests.post("https://test.instamojo.com/api/1.1/payment-requests/", data=payload, headers=headers)
            item = data.json()
            return HttpResponseRedirect(item['payment_request']['longurl'])
            # end instamojo

    else:
        form = PaymentForm()


    template = 'payment/index.html',
    name = request.user
    email_id = request.user.email
    context={
    'name':name,
    'email_id':email_id,
    'form':form
    }
    return render(request, template, context)

def pay(request):
    a = request.GET["payment_id"]
    b = request.GET["payment_request_id"]
    c = request.user
    d = request.user.email
    p = PaymentId(name=c, email=d, payment_id=a, payment_request_id=b)
    p.save()
    # subject = 'thank you'
    # message = 'Welcome to hostmud'
    # from_email = settings.DEFAULT_FROM_EMAIL
    # to_list = ['keshav@hostmud.com']
    # send_mail(subject,message,from_email,to_list,fail_silently=True)
    # print('sent')
    context={
    'a':a,
    'b':b
    }
    return render(request, 'payment/pay.html', context)

def subscribe(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = SubscribeForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            mail = form.cleaned_data['email']
            subject = 'New Subscriber'
            message = 'You have new subcriber to your website. The Email id is %s' % mail
        # body = "you received a message from %s mobile number is %s and query is %s" % (from_email, number,  message)
            from_email = mail
            to_list = ['apsrajput008@gmail.com']
            send_mail(subject,message,from_email,to_list,fail_silently=True)
            # print('sent')
            # print(form.cleaned_data['email'])
            return HttpResponseRedirect('/')
            # end instamojo

    else:
        form = SubscribeForm()


    template = 'payment/email.html',
    context={
    'form':form
    }
    return render(request, template, context)
