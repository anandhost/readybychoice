from django import forms
from .models import PaymentId

class PaymentForm(forms.Form):
    purpose = forms.CharField(label='purpose')
    amount = forms.IntegerField(label='amount')
    phone = forms.CharField(label='Your phone', max_length=10)
    email = forms.EmailField(label='Your email', max_length=100)
    name = forms.CharField(label='name', max_length=100)

class PaymentIdForm(forms.Form):
    name = forms.CharField(max_length=100)
    email = forms.EmailField(max_length=100)
    payment_id = forms.CharField(max_length=1000)
    payment_request_id = forms.CharField(max_length=1000)

class SubscribeForm(forms.Form):
    email = forms.EmailField(max_length=100)
