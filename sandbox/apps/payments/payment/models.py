from oscar.apps.payment.models import *  # noqa isort:skip
from django.db import models

class PaymentId(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    payment_id = models.CharField(max_length=1000)
    payment_request_id = models.CharField(max_length=1000)

    def __str__(self):
        return self.name
