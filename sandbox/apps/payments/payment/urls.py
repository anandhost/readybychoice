from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = (
	url(r'^$', views.index, name='index'),
	url(r'pay/$', views.pay, name="pay"),
	url(r'customer/$', views.payInfo, name="paymentInfo"),
	url(r'email/',views.subscribe, name='contact'),

)
