from oscar.apps.payment.admin import *  # noqa
from .models import PaymentId

admin.site.register(PaymentId)
